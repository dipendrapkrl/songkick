package np.pro.dipendra.songkick;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

public class SearchResultsActivity extends ListActivity {

	static final String KEY_ARTISTS = "artist";
	static final String KEY_ID = "id";
	static final String KEY_DISPLAY_NAME = "displayName";
	static final String KEY_URI = "uri";
	static final String KEY_ON_TOUR_UNTIL = "onTourUntil";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		String xml = getIntent().getStringExtra(MainActivity.INTENT_EXTRAS_XML);

		List<HashMap<String, String>> menuItems = new ArrayList<HashMap<String, String>>();

		addArtistsToHashmap(menuItems, xml);
		addItemsToList(menuItems);

	}

	private void addItemsToList(List<HashMap<String, String>> menuItems) {
		ListAdapter adapter = new SimpleAdapter(this, menuItems,
				R.layout.list_item, new String[] { KEY_ID, KEY_DISPLAY_NAME,
						KEY_URI, KEY_ON_TOUR_UNTIL }, new int[] { R.id.tvId,
						R.id.tvDisplayName, R.id.tvUri, R.id.tvOnTourUntil });

		setListAdapter(adapter);
	}

	private void addArtistsToHashmap(List<HashMap<String, String>> menuItems,
			String xml) {
		XMLParser parser = new XMLParser();

		Document doc = parser.getDomElement(xml);

		NodeList nl = doc.getElementsByTagName(KEY_ARTISTS);
		for (int i = 0; i < nl.getLength(); i++) {
			HashMap<String, String> map = new HashMap<String, String>();
			Element e = (Element) nl.item(i);

			map.put(KEY_ID, e.getAttribute(KEY_ID));
			map.put(KEY_DISPLAY_NAME, e.getAttribute(KEY_DISPLAY_NAME));
			map.put(KEY_URI, e.getAttribute(KEY_URI));
			map.put(KEY_ON_TOUR_UNTIL, e.getAttribute(KEY_ON_TOUR_UNTIL));

			menuItems.add(map);
		}

	}
}