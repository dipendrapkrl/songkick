package np.pro.dipendra.songkick;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	public static final String INTENT_EXTRAS_XML = "XML";
	public static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void clickHandler(View v) {
		switch (v.getId()) {
		case R.id.btnSearch:

			String query = ((EditText) findViewById(R.id.etSearchQuery))
					.getText().toString();
			if (query == null || query.trim().equals("")) {
				Toast.makeText(this, "Nothing to search", Toast.LENGTH_SHORT)
						.show();
				return;
			}

			if (!Utils.isOnline(this)) {
				Toast.makeText(this,
						"Internet is Required to Perform this operation",
						Toast.LENGTH_LONG).show();
				return;
			}
			new AsyncDownload().execute(query);

			break;
		default:
		}

	}

	private class AsyncDownload extends AsyncTask<String, String, String> {

		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Please Wait...");

			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {

			Log.v(TAG, "query is" + params[0]);
			String result = new ServerSideHelper().getXml(params[0]);
			return result;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			pDialog.dismiss();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			Log.v(TAG, "result=" + result);
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, SearchResultsActivity.class);
			intent.putExtra(INTENT_EXTRAS_XML, result);
			startActivity(intent);
		}

	}

}
